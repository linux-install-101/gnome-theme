echo "Cloning Repo\n"
git clone https://github.com/vinceliuice/Orchis-theme.git
echo "Running Install Script\n"
(cd Orchis-theme && ./install.sh)
echo "Removing Repo"
rm -rf Orchis-theme
